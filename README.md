### dk-calendar小程序日期打卡组件

该组件是以微信小程序为基础的已上线项目脱离片段开源（市面上小程序万变不离其中，其他小程序可自行修改标签wx）

**安装：**

```
git clone https://gitee.com/xiaokar/dk-calendar.git
```

**使用：**

1.将dk-calendar文件夹放进components文件夹内

2.页面json文件引入该组件

```
{
  "usingComponents": {
    "dk-calendar":"../../components/dk-calendar/dk-calendar"
  }
}
```

3.xxml页面文件使用：

```
<!-- 微打卡组件 -->
    <dk-calendar originActiveData="{{user_date_log}}" bind:selectDate="onSelectDate" size="m"></dk-calendar>
```

or

```
<!-- 默认打卡组件 -->
    <dk-calendar originActiveData="{{user_date_log}}" bind:selectDate="onSelectDate"></dk-calendar>
```

**参数方法相关**：

| 名称             | 描述             | 参数or方法 | 类型  | 例子                        |
| ---------------- | ---------------- | ---------- | ----- | --------------------------- |
| originActiveData | 已打卡的日期参数 | params     | Array | 例：[2021-11-02,2021-11-03] |
| selectDate       | 点击的日期方法   | Method     |       | 输出：yyyy-MM-dd            |

**最终效果（截图）：**

![](https://gitee.com/xiaokar/img/raw/master/dk-calendar.png)

**线上Demo参考：**

![](https://gitee.com/xiaokar/img/raw/master/mfdQr.png)

**开发原因：**

为满足定制需要，市面上又找不到车子和轮子，干脆自己撸了并分享有需要的人用

**更多**

该组件js日期均已使用yyyy/MM/dd格式连接解决ios不兼容问题，输出和输入为yyyy-MM-dd格式，简单易用，有问题可以联系我或者提出改进